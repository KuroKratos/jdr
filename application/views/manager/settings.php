<div class="row">
  <div class="col-4 offset-4">
    <div class="card z-depth-3 text-white elegant-color" style="border: 1px solid rgb(224,224,224);">
        <div class="card-body p-1 pl-2">
          Ajouter un objet
        </div>
        <div class="card-body py-2 px-0 border-top border-light form-inline">
          <div class="row">
            <input type="hidden" id="hidden_edit_id" value="-1">
            <div class="col-12 mb-2">
              <div class="col-3 float-left text-right">Environnement :</div>
              <div class="col-9 float-right">
                <select class="form-control input-sm" id="txt_game_env" style="border-radius: 0; height: 31px; border: 0; width: 100%">
                  <option value="-1">---</option>
                  <option value="jdr_azeroth">Azeroth</option>
                  <option value="jdr_naheulbeuk">Naheulbeuk</option>
                  <!-- <option value="jdr_gor">Game of roles</option> -->
                  <option value="jdr_cthulhu">Call of Cthulhu</option>
                </select>
              </div>
            </div>
            <div class="col-12 mb-2">
              <div class="col-3 float-left"></div>
              <div class="col-9 float-right">
                <div class="row">
                  <div class="col-6 float-left pr-1">
                    <button class="btn btn-sm btn-secondary px-3 m-0 col-12 waves-effect waves-light" onclick="resetItemForm()"><i class="fa fa-recycle mr-2"></i>Réinitialiser</button>
                  </div>
                  <div class="col-6 float-left pl-1">
                    <button class="btn btn-sm btn-primary px-3 m-0 col-12 waves-effect waves-light" id="btn_save"><i class="fa fa-floppy-o mr-2"></i>Enregistrer</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12">
              <div class="col-3 float-left"></div>
              <div class="col-9 float-right">
                <div class="color-block-dark danger-color-dark mb-0 p-1 text-center font-weight-bold" id="err" style="display: none;"></div>
                <div class="color-block-dark success-color-dark mb-0 p-1 text-center font-weight-bold" id="success" style="display: none;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  $("#btn_save").click(function () {
    $.ajax({
      type: "POST",
      url: base_url + "manager/update_game_env",
      data: {
        game_env: $("#txt_game_env").val()
      },
      dataType: "text",
      success: function (response) {
        if(response != 0) {
          location.reload();
        }
      }
    });
  });
</script>