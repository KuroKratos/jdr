<?php
namespace {

  class Manager extends MY_Controller {

    private $params;

    public function __construct() {
      parent::__construct();
      $this->load->model("m_char");
      $this->params = [
        "title"  => "Accueil",
        "char"   => $this->m_char->charList(),
        "assets" => ["Bootstrap4", "font-awesome", "dataTables"],
      ];
    }

    public function item () {
      $this->load->model("m_item");
      $this->params["categories"] = $this->m_item->getCategoryList();
      $this->params["characters"] = $this->m_char->allCharDetails();
      $this->params["title"]      = "Item Manager";
      $this->params["js"][]       = assets_url("js/item.js");
      $this->loadView(["manager/item"], $this->params);
    }

    public function settings() {
      $this->params["title"]      = "Paramètres";
      $this->loadView(["manager/settings"], $this->params);
    }

    public function update_game_env () {
      if(!empty($this->input->post("game_env"))) {
        $array = ["game_env" => $this->input->post("game_env")];
        $json = json_encode($array, JSON_PRETTY_PRINT);
        //$exec = scandir();
        $exec = file_put_contents(getcwd()."/assets/settings.json", $json);
      }
      print_r($exec ?? 0);
    }

    public function create_char() {
      if(!empty($this->input->post("name")) && !empty($this->input->post("sex")) && !empty($this->input->post("class")) && !empty($this->input->post("race"))) {
        $this->load->model("m_char","c");
        $ret = $this->c->create_character($this->input->post("name"),$this->input->post("sex"),$this->input->post("class"),$this->input->post("race"));
        if($ret > 0) {
          $ret2 = $this->c->create_empty_aptitudes($ret);
          echo $ret2;
        }
      }
      else {
        echo 0;
      }
    }

  }

}
