<?php
namespace {

  class Welcome extends MY_Controller {

    private $params;

    public function __construct() {
      parent::__construct();
      $this->load->model("m_char");
      $this->params = [
        "title"  => "Accueil",
        "char"   => $this->m_char->charList(),
        "assets" => ["Bootstrap4", "font-awesome", "dataTables"],
      ];
    }

    // Default view
    public function index() {
      $post = filter_input_array(INPUT_POST);
      if(!empty($post["ajax"]) && (int)$post["ajax"] === 1) {
        $this->load->view("home.html", $this->params);
      }
      else {
        $this->loadView(["home.html"], $this->params);
      }
    }

    // Game Master's dashboard with character summaries and other tools
    public function dashboard () {
      $this->params["characters"] = $this->m_char->allCharDetails();
      $this->params["title"]      = "Dashboard";
      $this->params["js"][]       = assets_url("js/dashboard.js");
      $this->loadView(["dashboard"], $this->params);
    }

    // Character sheet
    public function charsheet($char=null) {
      if(!empty($char)) {
        $this->params["character"] = $this->m_char->charDetails(urldecode($char));
        $this->params["title"]     = urldecode($char);
        $this->params["js"][]      = assets_url("js/charsheet.js");
        $post                      = filter_input_array(INPUT_POST);
        if(!empty($post["ajax"]) && (int)$post["ajax"] === 1) {
          $this->load->view("charsheet", $this->params);
        }
        else {
          $this->loadView(["charsheet"], $this->params);
        }
      }
    }

  }

}
