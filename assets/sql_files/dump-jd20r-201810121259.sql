-- MySQL dump 10.16  Distrib 10.3.9-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: jd20r
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aptitude_category`
--

DROP TABLE IF EXISTS `aptitude_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aptitude_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `color` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aptitude_category`
--

LOCK TABLES `aptitude_category` WRITE;
/*!40000 ALTER TABLE `aptitude_category` DISABLE KEYS */;
INSERT INTO `aptitude_category` VALUES (1,'Combat','gold'),(2,'Connaissances','skyblue'),(3,'Générales','burlywood');
/*!40000 ALTER TABLE `aptitude_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aptitude_points`
--

DROP TABLE IF EXISTS `aptitude_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aptitude_points` (
  `apt_id` int(11) NOT NULL,
  `char_id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aptitude_points`
--

LOCK TABLES `aptitude_points` WRITE;
/*!40000 ALTER TABLE `aptitude_points` DISABLE KEYS */;
INSERT INTO `aptitude_points` VALUES (4,1,15),(5,1,15),(6,1,0),(7,1,0),(8,1,10),(9,1,10),(2,1,8),(1,1,10),(3,1,2),(10,1,14),(11,1,15),(12,1,0),(13,1,0),(14,1,10),(15,1,4),(16,1,10),(17,1,11),(18,1,8),(19,1,5),(21,1,0),(22,1,0),(23,1,13),(4,2,0),(5,2,5),(6,2,15),(7,2,5),(8,2,25),(9,2,0),(2,2,2),(1,2,10),(3,2,8),(10,2,22),(11,2,0),(12,2,0),(13,2,0),(14,2,0),(15,2,28),(16,2,0),(17,2,6),(18,2,10),(19,2,24),(21,2,4),(22,2,0),(23,2,6),(4,3,15),(5,3,2),(6,3,15),(7,3,1),(8,3,2),(9,3,0),(2,3,5),(1,3,5),(3,3,10),(10,3,16),(11,3,0),(12,3,0),(13,3,18),(14,3,0),(15,3,16),(16,3,0),(17,3,0),(18,3,0),(19,3,0),(21,3,25),(22,3,0),(23,3,0),(4,4,2),(5,4,32),(6,4,12),(7,4,2),(8,4,0),(9,4,2),(2,4,3),(1,4,8),(3,4,9),(10,4,2),(11,4,2),(12,4,13),(13,4,0),(14,4,12),(15,4,17),(16,4,2),(17,4,3),(18,4,3),(19,4,19),(21,4,17),(22,4,1),(23,4,3),(4,5,15),(5,5,3),(6,5,8),(7,5,8),(8,5,6),(9,5,10),(2,5,7),(1,5,7),(3,5,6),(10,5,1),(11,5,16),(12,5,1),(13,5,1),(14,5,0),(15,5,10),(16,5,0),(17,5,1),(18,5,0),(19,5,15),(21,5,5),(22,5,25),(23,5,25),(4,6,15),(5,6,6),(6,6,17),(7,6,0),(8,6,12),(9,6,0),(2,6,6),(1,6,4),(3,6,10),(10,6,14),(11,6,20),(12,6,4),(13,6,20),(14,6,12),(15,6,15),(16,6,0),(17,6,0),(18,6,0),(19,6,0),(21,6,10),(22,6,0),(23,6,0),(20,4,6),(20,1,10),(20,6,5);
/*!40000 ALTER TABLE `aptitude_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aptitudes`
--

DROP TABLE IF EXISTS `aptitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aptitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `category` int(11) NOT NULL,
  `stat_1` text,
  `stat_2` text,
  `modificator` int(11) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `aptitudes_category_IDX` (`category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aptitudes`
--

LOCK TABLES `aptitudes` WRITE;
/*!40000 ALTER TABLE `aptitudes` DISABLE KEYS */;
INSERT INTO `aptitudes` VALUES (1,'Endurance',1,'con','pou',0,'Mesure la constitution du personnage et sa capacité à survivre à la faim, la soif, au froid, ou aux maladies et infections.'),(2,'Esquive',1,'dex',NULL,10,'Permet d’éviter les périls physiques, comme un coup de hache.'),(3,'Persévérance',1,'pou',NULL,10,'Mesure la volonté de votre personnage, sa résistance à la magie et aux tentatives visant à l’influencer.'),(4,'Connaissance de la nature',2,'int',NULL,10,'Prédire le temps qu’il fera, reconnaitre et s’occuper des animaux et des plantes, géologie et survie dans le monde naturel.'),(5,'Connaissances (autre)',2,'int',NULL,0,'Mesure la volonté de votre personnage, sa résistance à la magie et aux tentatives visant à l’influencer.'),(6,'Culture d\'origine',2,'int',NULL,30,'Ce qu’un personnage sait de l’histoire, de la politique, de la géographie de sa région et de sa société d’origine.'),(7,'Culture (autre)',2,'int',NULL,0,'Ce qu’un personnage sait de l’histoire, de la politique et de la géographie d’une contrée étrangère.'),(8,'Langue maternelle',2,'int',NULL,50,'La capacité à parler et à écrire dans sa propre langue.'),(9,'Langue (autre)',2,'int',NULL,0,'La facilité avec laquelle un personnage peut parler une langue étrangère, voire lire et écrire dans cette langue.'),(10,'Artisanat',3,'int',NULL,10,'Permet de créer des choses, comme des pots, des armes ou des bâtiments.'),(11,'Athlétisme',3,'dex','for',0,'Courir, grimper, sauter et soulever des poids.'),(12,'Conduite',3,'dex','int',0,'Diriger des véhicules des périodes antiques et médiévales, tels que les charrues, les charriots, et les charrettes.'),(13,'Equitation',3,'dex','pou',0,'Tenter des manoeuvres risquées à dos de bêtes comme les chameaux ou les chevaux, voire de créatures plus fantastiques telles que les hippogriffes.'),(14,'Guérison',3,'int',NULL,10,'Soigner des blessures et des maladies en utilisant les premiers soins et la chirurgie.'),(15,'Influence',3,'cha',NULL,10,'Persuader quelqu’un de faire quelque chose qui est contraire à ses intérêts.'),(16,'Ingénierie',3,'int',NULL,10,'Les constructions de grande ampleur et les engins de siège sont couverts par cette compétence.'),(17,'Mécanismes',3,'int','dex',0,'Manipuler des serrures et toute chose avec des éléments mouvants complexes.'),(18,'Navigation',3,'cha','pou',0,'Diriger des navires et des barques.'),(19,'Commerce',3,'int','cha',0,'Évaluer et vendre des marchandises.'),(20,'Perception',3,'int','pou',0,'Repérer des objets cachés et de petits détails de l’environnement.'),(21,'Performance',3,'cha',NULL,10,'Jouer un rôle dans une pièce de théâtre, chanter, danser, jouer d’un instrument...'),(22,'Sens de la rue',3,'cha','pou',0,'Détermine comment un personnage opère dans un environnement urbain. À utiliser notamment pour trouver de l’information, s’orienter dans les rues, et trouver des recéleurs prêts à vendre des biens volés.'),(23,'Tromperie',3,'dex','int',0,'Faire preuve de discrétion, se cacher et faire du vol à la tire.');
/*!40000 ALTER TABLE `aptitudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bestiary`
--

DROP TABLE IF EXISTS `bestiary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bestiary` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `hp` int(11) NOT NULL,
  `attack` varchar(16) NOT NULL,
  `defense` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bestiary`
--

LOCK TABLES `bestiary` WRITE;
/*!40000 ALTER TABLE `bestiary` DISABLE KEYS */;
INSERT INTO `bestiary` VALUES (0,'Aucun ennemi',0,'---',0),(1,'Dinde sauvage',10,'1d4',0),(2,'Zombie décérébré',20,'1d6',0),(3,'Ghoule déshéritée',30,'1d8',0),(4,'Âme égarée',50,'1d10',1),(5,'Jeune charognard',25,'1d6',0),(6,'Charognard pouilleux',40,'1d8',0),(7,'Chef de meute charognard',65,'1d10',1),(8,'Jeune tisse-nuit',30,'1d6',0),(9,'Araignée tisse-nuit',50,'1d8',0),(10,'Matriarche tisse-nuit',100,'1d10+2',2),(11,'Gregor Agamand',100,'1d10+2',2),(12,'Nissa Agamand',100,'1d10+2',2),(13,'Thurman Agamand',100,'1d10+2',2),(14,'Devlin Agamand',120,'1d10+2',2);
/*!40000 ALTER TABLE `bestiary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character`
--

DROP TABLE IF EXISTS `character`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character` (
  `char_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `sex` char(1) NOT NULL DEFAULT 'M',
  `class` varchar(128) NOT NULL,
  `race` varchar(64) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '1',
  `traits` text NOT NULL,
  `strength` tinyint(4) NOT NULL DEFAULT '50',
  `intellect` tinyint(4) NOT NULL DEFAULT '50',
  `dexterity` tinyint(4) NOT NULL DEFAULT '50',
  `constitution` tinyint(4) NOT NULL DEFAULT '50',
  `willpower` tinyint(4) NOT NULL DEFAULT '50',
  `size` tinyint(4) NOT NULL DEFAULT '50',
  `charisma` tinyint(4) NOT NULL DEFAULT '50',
  `luck` tinyint(4) NOT NULL DEFAULT '50',
  `alignement` tinyint(4) NOT NULL DEFAULT '50',
  `hp_cur` mediumint(9) NOT NULL DEFAULT '210',
  `hp_max` mediumint(9) NOT NULL DEFAULT '210',
  `pp_cur` mediumint(9) NOT NULL DEFAULT '210',
  `pp_max` mediumint(9) NOT NULL DEFAULT '210',
  `gold` mediumint(9) NOT NULL DEFAULT '0',
  `story` text NOT NULL,
  PRIMARY KEY (`char_id`),
  KEY `char_id` (`char_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character`
--

LOCK TABLES `character` WRITE;
/*!40000 ALTER TABLE `character` DISABLE KEYS */;
INSERT INTO `character` VALUES (1,'Dunstann','F','4','Worgen',1,'Snif sinf je sens votre peur face à moi, n\'ayez crainte, vous n\'êtes pas des proies assez intéressantes.',40,40,40,40,40,40,40,50,25,160,160,160,160,0,'Que dire mis à part que je suis un loup solitaire, à la recherche de compagnons que je n\'aurais pas envie de manger. Bref, l\'estomac toujours vide, et ne pouvant rester sur place je me contente d\'aller de l\'avant, errant sans but. Et... ohhhh... un papillon!!!'),(2,'Graïn','M','12','Nain',1,'',90,40,40,90,60,50,60,50,50,160,280,160,200,0,'Issu des vallées enneigées d\'Alterac, Graïn Barbe d\'Ambre est un membre d\'une lignée dont le rôle fut pendant des siècles de protéger Alterac de sorte à garantir les frontières entre le royaume d\'Hurlevent et celui de Lordaeron. Toujours partant pour déguster une bonne bière autour d\'une table débordant de viandes en tout genre et de pâté en croûte, ce joyeux fêtard saura trouver l\'étincelle d\'optimisme dont tout groupe à besoin même lors des heures les plus sombres. Attention cependant à ne pas le laisser trop longtemps en compagnie des elfes dont il garde une vieille rancœur culturelle... Ah! Et aussi des paladins... Car la bubulle encore, ça peut passer... Mais les épées c\'est bon pour les donzelles, autant se trouver une hache!'),(3,'Valadriel','F','8','Draeneï',1,'',70,90,60,40,70,40,60,50,50,160,160,320,320,0,'Je.. Je ne me souviens pas de ma vie d\'avant, ni mon nom, ni mon âge, tout ce que je sais, c\'est que je me suis réveillé dans un temple dont je ne connais pas le nom, j\'ai été recueillis et éduqué par l\'ordre des prêtres du temple, ils m\'ont baptisé Valadriel, je le sais car ce nom était gravé sur ma porte et ma place à table, j\'ai passé 5 ans là bas, dans le silence, car le vœu de silence était obligatoire et non un choix, j\'ai décidé de partir vivre ma vie et mettre mon savoir et mes pouvoirs au service de l\'Alliance.  '),(4,'Raoul','M','5','Kul-Tiran',1,'',45,90,45,50,80,55,65,50,50,200,210,340,340,0,'Je voulais pas y aller dans cette école de magie moi !\n\nJ\'ai jamais aimé ça, me concentrer, mais je voulais pas non plus me faire virer puis bastonner par le vieux, alors j\'ai appris. \n\nMais j\'ai la flemme... Et puis j\'ai faim...\n\nMaintenant que je suis un mage diplômé, il m\'a fichu à la porte, alors je dois me débrouiller pour me remplir le bide et les poches.\n\nIl parait que le moyen le plus simple c\'est l\'aventure alors j\'ai rejoint un groupe de glands aussi paumés, nuls et fauchés que moi et on va voir ce qu\'on arrive à faire...\n\nPurée, j\'ai la dalle....'),(5,'Kargrinn','M','7','Nain',1,'',80,40,40,80,40,75,75,50,50,160,310,160,160,9,'Il est né de la BRIQUE bien après ses semblables... Il ne sait pas ce qu\'il fait là mais il compte bien le découvrir...\nArdent défenseur des lolis qu\'il prend pour des déesses il ne cesse de les regarder avec admiration comme quand il reçu son premier bâton et sa roue.\nIl aime prendre des coups... hum des cooooouuuuupppsss... hum hum pour ensuite en rendre le double.\nIl n\'aime pas trop les guerriers, ces imbéciles qui tapent dés que sa bouge, \"autant qu\'il meurt ça ferai déjà un con de moins à heal sur Azeroth\" qu\'il disait !\nPour finir avec sa phrase fétiche \"J\'savais bien que t\'étais une Loli !\"'),(6,'Ednas','F','3','Kal\'dorei',1,'',30,90,70,50,70,60,60,50,50,220,220,320,320,0,'La vie d\'Ednas était paisible jusqu\'à présent au sein de Teldrassil,\nMais la guerre des épines a tout remis en question, Ednas se retrouve forcer à quitter sa famille et tout ce qu\'elle a pu connaître jusqu\'alors pour partir à la découverte de l\'inconnu,\nAdepte du déambulage en forêt et de la chasse aux papillons, elle a tout à réapprendre.');
/*!40000 ALTER TABLE `character` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_m` varchar(32) NOT NULL,
  `name_f` varchar(32) NOT NULL,
  `color` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,'Chevalier de la mort','Chevalier de la mort','rgb(196 ,31 ,59)'),(2,'Chasseur de démons','Chasseuse de démons','rgb(163 ,48 ,201)'),(3,'Druide','Druidesse','rgb(255 ,125 ,10)'),(4,'Chasseur','Chasseresse','rgb(171 ,212 ,115)'),(5,'Mage','Mage','rgb(105 ,204 ,240)'),(6,'Moine','Moniale','rgb(0 ,255 ,150)'),(7,'Paladin','Paladin','rgb(245,140,186)'),(8,'Prêtre','Prêtresse','rgb(255 ,255 ,255)'),(9,'Voleur','Voleuse','rgb(255 ,245 ,105)'),(10,'Chaman','Chamane','rgb(0 ,112 ,222)'),(11,'Démoniste','Démoniste','rgb(148 ,130 ,201)'),(12,'Guerrier','Guerrière','rgb(199 ,156 ,110)');
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(256) NOT NULL,
  `quantity` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES (6,5,'Etoffe de lin','Tissu',1,0),(7,5,'Potion 30 HP','Rend 30 HP à l\'utilisation',1,0),(28,1,'lance-pierre','lace pierre avec un lien de cuir et un bâton',1,0),(30,1,'Bâon parlant/vide','Bâton enroulé dune toile de tente, parle et murmure,impregné de vide',1,0),(31,4,'Dessous féminins','Oulala',1,0),(32,4,'Babioles en or','Coupe, Assiette, Epée',1,0),(33,5,'Ration de soldat','?',2,0);
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` tinyint(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `bonus_value` tinyint(4) NOT NULL,
  `bonus_stat` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,6,'Epée Doncule','[1d10]',5,'dex'),(2,6,'Epée Zervatyf','[1d8]',-5,'cha');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_category`
--

DROP TABLE IF EXISTS `item_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_category`
--

LOCK TABLES `item_category` WRITE;
/*!40000 ALTER TABLE `item_category` DISABLE KEYS */;
INSERT INTO `item_category` VALUES (1,'Divers'),(2,'Consommables'),(3,'Artisanat'),(4,'Outils'),(5,'Quête'),(6,'Armes'),(7,'Armures');
/*!40000 ALTER TABLE `item_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `music` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Clairières de Tirisfal','https://www.youtube.com/watch?v=NoIIlbUpYtE'),(2,'Fossoyeuse','https://www.youtube.com/watch?v=VRND4YhN-lU');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region_bestiary`
--

DROP TABLE IF EXISTS `region_bestiary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region_bestiary` (
  `region_id` int(11) NOT NULL,
  `bestiary_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region_bestiary`
--

LOCK TABLES `region_bestiary` WRITE;
/*!40000 ALTER TABLE `region_bestiary` DISABLE KEYS */;
INSERT INTO `region_bestiary` VALUES (1,1,50),(1,2,100),(1,3,50),(1,4,30),(1,5,100),(1,6,50),(1,7,30),(1,8,100),(1,9,50),(1,10,0),(1,11,0),(1,12,0),(1,13,0),(1,14,0),(1,0,500);
/*!40000 ALTER TABLE `region_bestiary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `char_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `value` text,
  `effect` varchar(44) NOT NULL,
  `worth` int(11) NOT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,1,'Tir à l\'arc','1D6','Tire une flèche',0),(2,1,'Visée','1D8 + 4','1 tour de préparation, ne peut rater',30),(3,2,'Frappe','1D6','Frappe avec son arme en main droite.',0),(4,2,'Peau de pierre','-','Réduit les dégâts subits de 50%  pour 1 tour',20),(5,3,'Coup de bâton','1D4','Donne un coup de bâton.',0),(6,3,'Don des Naaru','1D20 + 10','Rend quelques points de vie.',30),(7,4,'Coup de bâton','1D4','Donne un coup de bâton.',0),(8,4,'Trait de glace','1D8','Peut geler la cible à 15%',30),(9,5,'Frappe','1D6','Frappe avec son arme en main droite.',0),(10,5,'Consécration','1D4 / tour','Dégâts autour de lui durant 4 tours',10),(11,6,'Coup de bâton','1D4','Donne un coup de bâton.',0),(12,6,'Courroux Solaire','1D8','Boule d\'énergie solaire',30);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistics`
--

DROP TABLE IF EXISTS `statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` text NOT NULL,
  `display_name` text NOT NULL,
  `abreviation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistics`
--

LOCK TABLES `statistics` WRITE;
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
INSERT INTO `statistics` VALUES (1,'strength','Force','for'),(2,'intellect','Intelligence','int'),(3,'dexterity','Dextérité','dex'),(4,'constitution','Constitution','con'),(5,'willpower','Pouvoir','pou'),(6,'size','Taille','tai'),(7,'charisma','Charisme','cha'),(8,'luck','Chance','luk');
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'jd20r'
--
